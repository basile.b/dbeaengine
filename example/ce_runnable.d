/+ dub.sdl:
name "dbeaengine-example1"
dependency "dbeaengine" path="../"
+/
module runnable;


/**
 * Coedit - runnable module
 * - beaengine must be setup in the library manager.
 * - click menu Run, Compile file & run.
 */

import std.stdio;
import disassembler;

void main(string[] args)
{
    // stores the names to get a hint for the calls
    symbolTable.add!(writeln!string);
    symbolTable.add!main;
    symbolTable.add!prettyDisasm;

    // let's be crazy...
    symbolTable.add!(symbolTable.add!main, "add1");
    symbolTable.add!(symbolTable.add!(symbolTable.add!main, "add1"), "add2");
    symbolTable.add!(symbolTable.add!(symbolTable.add!(symbolTable.add!main, "add1"), "add2"), "add3");

    version(Windows) eolMode = EolMode.cr;

    prettyDisasm(&main, 2).writeln;
}
