# D-beaengine [![CI Status](https://travis-ci.org/Basile-z/dbeaengine.svg)](https://travis-ci.org/Basile-z/dbeaengine) [![codecov](https://codecov.io/gh/Basile-z/iz/branch/master/graph/badge.svg)](https://codecov.io/gh/Basile-z/dbeaengine)

_dbeaengine_ is a simple [BeaEngine](https://github.com/BeaEngine/beaengine) interface for the [D programming language](http://dlang.org/). The _dbeaengine_ is an X86 and X86_64 library that can be used to disassemble X86 and X86_64 byte-code.

The programming interface is simple and easy (a single function named ``Disasm()`` ).
Confer with the official website for more information.

This D binding doesn't add a layout over the original API but the `enum`, `struct` and
their members names are more D-friendly. Additionally, safety is introduced by turning `const` every data set by _Bea_, which avoids any unintentional modification of the results.

## Repository structure

- ``\beaengine``: contains the original C sources and the shell scripts to build _beaengine_ as an object.
- ``\example``: contains two runnables samples, they can be run as dub scripts or as Coedit runnables.
- ``\src``: contains the d interface and a disassembler as library.
- ``\objects``: contains the _*.obj_ and _*.o_ objects generated for the C library, by format and by architecture.

## Setup

generate the static library either with the DUB descirption or with the Coedit project (dbeaengine.ce).

## Library disassembler.

Using the Coedit IDE, the module `disassembler` is amazingly easy to use and efficient when there is a need to directly see the code generated for a function. For example the following functions are disassembled in a single click thanks to Coedit runnable module system! This is a killer feature when you learn D or when there is a need to inspect the back-end production.

![](https://github.com/Basile-z/dbeaengine/blob/master/example/sample2.png?raw=true)

This feature is actually comparable to what [the online disassemblers](https://wiki.dlang.org/Online_compilers) do.
    
## Usage

the most simple example:

```D
import std.stdio;
import disassembler;

void main(string[] args)
{
    writeln(prettyDisasm(&main, 2));
}
```

prints the code generated for `__Dmain` and its calls up to one level.


## Other

- tested under windows 32 bit, linux 32 bit and  linux 64 bit.
- original BeaEngine license: LGPL v3. see _beaengine_ folder.
