module beaengine;

//TODO: integrate documentation

// Instruction ----------------------------------------------------------------+

/// Enumerates the possible consequences the instruction have on an Eflag.
enum FlagState : ubyte {
    /// the flag is tested
    TE_ = 1, 
    /// the falg is modified
    MO_ = 2,
    /// the flag is reset
    RE_ = 4,
    /// the flag is set
    SE_ = 8,
    /// undefined behaviour
    UN_ = 0x10, 
    /// the flag is restored to its prior state
    PR_ = 0x20  
}

/// This structure gives information on the EFLAG registers.
struct Eflags {
align(1): const:
    FlagState OF_;
    FlagState SF_;
    FlagState ZF_;
    FlagState AF_;
    FlagState PF_;
    FlagState CF_;
    FlagState TF_;
    FlagState IF_;
    FlagState DF_;
    FlagState NT_;
    FlagState RF_;
    private ubyte alignment;
}

/// Enumerates the possible instruction sets
enum InstrSet : ushort {
    GENERAL_PURPOSE_INSTRUCTION   =    0x1,
    FPU_INSTRUCTION               =    0x2,
    MMX_INSTRUCTION               =    0x4,
    SSE_INSTRUCTION               =    0x8,
    SSE2_INSTRUCTION              =   0x10,
    SSE3_INSTRUCTION              =   0x20,
    SSSE3_INSTRUCTION             =   0x40,
    SSE41_INSTRUCTION             =   0x80,
    SSE42_INSTRUCTION             =  0x100,
    SYSTEM_INSTRUCTION            =  0x200,
    VM_INSTRUCTION                =  0x400,
    UNDOCUMENTED_INSTRUCTION      =  0x800,
    AMD_INSTRUCTION               = 0x1000,
    ILLEGAL_INSTRUCTION           = 0x2000,
    AES_INSTRUCTION               = 0x4000,
    CLMUL_INSTRUCTION             = 0x8000,
}

/// Enumerates the possible instruction categories
enum InstrCat : ushort {
    DATA_TRANSFER = 0x1,
    ARITHMETIC_INSTRUCTION,
    LOGICAL_INSTRUCTION,
    SHIFT_ROTATE,
    BIT_UInt8,
    CONTROL_TRANSFER,
    STRING_INSTRUCTION,
    InOutINSTRUCTION,
    ENTER_LEAVE_INSTRUCTION,
    FLAG_CONTROL_INSTRUCTION,
    SEGMENT_REGISTER,
    MISCELLANEOUS_INSTRUCTION,
    COMPARISON_INSTRUCTION,
    LOGARITHMIC_INSTRUCTION,
    TRIGONOMETRIC_INSTRUCTION,
    UNSUPPORTED_INSTRUCTION,
    LOAD_CONSTANTS,
    FPUCONTROL,
    STATE_MANAGEMENT,
    CONVERSION_INSTRUCTION,
    SHUFFLE_UNPACK,
    PACKED_SINGLE_PRECISION,
    SIMD128bits,
    SIMD64bits,
    CACHEABILITY_CONTROL,
    FP_INTEGER_CONVERSION,
    SPECIALIZED_128bits,
    SIMD_FP_PACKED,
    SIMD_FP_HORIZONTAL ,
    AGENT_SYNCHRONISATION,
    PACKED_ALIGN_RIGHT  ,
    PACKED_SIGN,
    PACKED_BLENDING_INSTRUCTION,
    PACKED_TEST,
    PACKED_MINMAX,
    HORIZONTAL_SEARCH,
    PACKED_EQUALITY,
    STREAMING_LOAD,
    INSERTION_EXTRACTION,
    DOT_PRODUCT,
    SAD_INSTRUCTION,
    ACCELERATOR_INSTRUCTION,    /* crc32, popcnt (sse4.2) */
    ROUND_INSTRUCTION
}

/// Enumerates the possible branche types
enum BranchType : int {
    JO = 1,
    JC = 2,
    JE = 3,
    JA = 4,
    JS = 5,
    JP = 6,
    JL = 7,
    JG = 8,
    JB = 2,       /* JC == JB */
    JECXZ = 10,
    JMP = 11,
    CALL= 12,
    RET = 13,
    JNO = -1,
    JNC = -2,
    JNE = -3,
    JNA = -4,
    JNS = -5,
    JNP = -6,
    JNL = -7,
    JNG = -8,
    JNB = -2      /* JNC == JNB */
}

/// Output structure describing an instruction
struct InstrType {
align(1): const:
    /// instruction category, see the InstrCat enum.
    InstrCat     category;
    /// instruction set, see the InstrSet enum.
    InstrSet     set;
    /// the instruction opcode, up to 3 bytes.
    int          opcode;
    /// the instruction as text, up to 16 chars but null terminated.
    char[16]     mnemonic;
    /// the instruction branch type, only valid when the category equal to CONTROL_TRANSFER. 
    BranchType   branch;
    /// the eflags modifications, see struct Eflags and enum FlagState.
    Eflags       flags;
    /// destination address of a branch instruction if <> 0. 
    ulong        addrValue;
    /// If the instruction uses a constant, this immediat value is stored here.
    long         immediat;
    /// can be interpreted using bit masking according to the ArgType enum
    ArgType      implicitModifiedRegs;
}

// -----------------------------------------------------------------------------
// Argument -------------------------------------------------------------------+

enum SegmentReg {
    ESReg = 1,
    DSReg = 2,
    FSReg = 3,
    GSReg = 4,
    CSReg = 5,
    SSReg = 6
}

enum ArgType : uint {
    NO_ARGUMENT   = 0x10000000,
    REGISTER_TYPE = 0x20000000,
    MEMORY_TYPE   = 0x40000000,
    CONSTANT_TYPE = 0x80000000,
    
    MMX_REG       = 0x10000,
    GENERAL_REG   = 0x20000,
    FPU_REG       = 0x40000,
    SSE_REG       = 0x80000,
    CR_REG        = 0x100000,
    DR_REG        = 0x200000,
    SPECIAL_REG           = 0x400000,
    MEMORY_MANAGEMENT_REG = 0x800000,
    SEGMENT_REG           = 0x1000000,
    
    RELATIVE_ = 0x4000000,
    ABSOLUTE_ = 0x8000000,
    
    READ  = 0x1,
    WRITE = 0x2,
    
    // if ... & 0xF000F0000 = REGISTER_TYPE + FPU_REG then LowWord indicates REGX
    REG0 = 0x1,     //( RAX / MM0 / ST0 / XMM0  / CR0  / DR0  / GDTR / ES )
    REG1 = 0x2,     //( RCX / MM1 / ST1 / XMM1  / CR1  / DR1  / LDTR / CS )
    REG2 = 0x4,     //( RDX / MM2 / ST2 / XMM2  / CR2  / DR2  / IDTR / SS )
    REG3 = 0x8,     //( RBX / MM3 / ST3 / XMM3  / CR3  / DR3  / TR   / DS )
    REG4 = 0x10,    //( RSP / MM4 / ST4 / XMM4  / CR4  / DR4  / ---- / FS )
    REG5 = 0x20,    //( RBP / MM5 / ST5 / XMM5  / CR5  / DR5  / ---- / GS )
    REG6 = 0x40,    //( RSI / MM6 / ST6 / XMM6  / CR6  / DR6  / ---- / -- )
    REG7 = 0x80,    //( RDI / MM7 / ST7 / XMM7  / CR7  / DR7  / ---- / -- )
    REG8 = 0x100,   //( R8  / --- / --- / XMM8  / CR8  / DR8  / ---- / -- )
    REG9 = 0x200,   //( R9  / --- / --- / XMM9  / CR9  / DR9  / ---- / -- )
    REG10 = 0x400,  //( R10 / --- / --- / XMM10 / CR10 / DR10 / ---- / -- )
    REG11 = 0x800,  //( R11 / --- / --- / XMM11 / CR11 / DR11 / ---- / -- )
    REG12 = 0x1000, //( R12 / --- / --- / XMM12 / CR12 / DR12 / ---- / -- )
    REG13 = 0x2000, //( R13 / --- / --- / XMM13 / CR13 / DR13 / ---- / -- )
    REG14 = 0x4000, //( R14 / --- / --- / XMM14 / CR14 / DR14 / ---- / -- )
    REG15 = 0x8000, //( R15 / --- / --- / XMM15 / CR15 / DR15 / ---- / -- )
}

/// This structure gives information on the memory access type, according to the formula BaseRegister + IndexRegister*Scale + Displacement]
struct MemType {
align(4):
    const(int) BaseRegister;
    const(int) IndexRegister;
    // 1, 2, 4 or 8
    const(int) Scale;
    const(long) Displacement;
}

/// Describes an instruction argument
struct Argument {
align(1): const:
    /// the argument as text, up to 64 chars but null terminated
    char[64]    mnemonic;
    ArgType     type;
    int         size;
    int         position;
    uint        accessMode;
    MemType     memory;
    SegmentReg  segmentReg; // only if arg1 or arg2
}

// -----------------------------------------------------------------------------
// High end -------------------------------------------------------------------+

/**
 * Specify the architecture used for the decoding
 */
enum Archi : uint {
    ia32    = 0,
    a8086   = 16,
    intel64 = 64
}

/**
 * This field allows you to define some display options. 
 * You can specify the syntax : masm, nasm, goasm or AT&T. 
 * You can specify the number format you want to use : prefixed numbers or suffixed ones. 
 * You can even add a tabulation between the mnemonic and the first operand or 
 * display the segment registers used by the memory addressing. 
 */
enum DisasmOpts : ulong {
    noTabs          = 0x0,
    Tabs            = 0x1,
    synMasm         = 0x000,
    synGoAsm        = 0x100,
    synNasm         = 0x200,
    synAT           = 0x400,
    prefixedNumeral = 0x10000,
    suffixedNumeral = 0x00000,
    showSegmentRegs = 0x01000000
}

enum LockPrefix : ubyte{
    NotUsedPrefix      = 0,
    InUsePrefix        = 1,
    SuperfluousPrefix  = 2,
    InvalidPrefix      = 4,
    MandatoryPrefix    = 8
}

struct Rex {
align(1):
    ubyte W_;
    ubyte R_;
    ubyte X_;
    ubyte B_;
    ubyte state;
}

struct PrefixInfo {
align(1): const:
    int Number;
    int NbUndefined;
    LockPrefix lock;
    ubyte OperandSize;
    ubyte AddressSize;
    ubyte RepnePrefix;
    ubyte RepPrefix;
    ubyte FSPrefix;
    ubyte SSPrefix;
    ubyte GSPrefix;
    ubyte ESPrefix;
    ubyte CSPrefix;
    ubyte DSPrefix;
    ubyte BranchTaken;
    ubyte BranchNotTaken;
    Rex   rex;
    char[2] alignment;
}   

/**
 * This structure is used to store the mnemonic, source and destination operands. 
 * You just have to specify the address where the engine has to make the analysis.
 */
struct DisasmParams {
align(1):
public:
    /// input, the entry point
    void*       eip;
    /// input, when set CALL - JMP - JX/JNX - LOOP are based on this value, not eip
    ulong       virtualAddress;
    /// input, limits the possible instruction length. the default value, 15, is also the longest possible.
    uint        securityBlock = 15;
    /// output, instruction and argument as text, up to 64 chars but null terminated.
    const char[64]    asString;
    /// input, specifies the target architecture, according to the enum Archi.
    Archi       archi;
    /// input, sepcifies the result format.
    DisasmOpts  options;
const:
    /// output, describes the instruction. 
    InstrType   instruction;
    /// output, optional, describes the first argument.
    Argument    arg1;
    /// output, optional, describes the second argument.
    Argument    arg2;
    /// output, optional, describes the third argument.
    Argument    arg3;
    /// output, PrefixInfo containing an exhaustive list of used prefixes.
    PrefixInfo  prefix;
private: 
    uint[40] Reserved_;
}

// -----------------------------------------------------------------------------
// Functions ------------------------------------------------------------------+

extern(C) {
    private int Disasm (DisasmParams * params);
    const(ubyte*) BeaEngineVersion();
    const(ubyte*) BeaEngineRevision();
}

/**
 * The Disasm function allows you to decode all instructions coded according to 
 * the rules of IA-32 and Intel 64 architectures. It makes a precise analysis of 
 * the focused instruction and sends back a complete structure that is usable to 
 * make data-flow and control-flow studies. Disasm is able to decode all the 
 * documented intel instructions (standard instructions, FPU, MMX, SSE, SSE2, 
 * SSE3, SSSE3 ,SSE4.1, SSE4.2, VMX, CLMUL and AES technologies) and undocumented 
 * ones like SALC, FEMMS (instruction AMD), HINT_NOP, ICEBP and aliases.
 * 
 * Params:
 * params = the parameter to disassemble at a particular address. The structs contains
 * the result after the call.
 *
 * Return:
 * If the operation is sucessful then the result is equal to the length of the instruction, so a value between 1 and 15.
 * If the operation fails then the result is either equal to SpecialInfo.UNKNOWN_OPCODE or to SpecialInfo.OUT_OF_BLOCK. 
 *
 * Examples:
 * ---
 * DisasmParams p;
 * p.eip = &myFunction;
 * disassemble(&p);
 * writeln(p.asString);
 * ---
 */
int disassemble(DisasmParams * params)
{
    return Disasm(params);
}

// -----------------------------------------------------------------------------
// Other ----------------------------------------------------------------------+

static enum LowPosition    = 0;
static enum HighPosition   = 1;

enum SpecialInfo
{
  UNKNOWN_OPCODE = -1,
  OUT_OF_BLOCK = 0,

  /* === mask = 0xff */
  NoTabulation      = 0x00000000,
  Tabulation        = 0x00000001,

  /* === mask = 0xff00 */
  MasmSyntax        = 0x00000000,
  GoAsmSyntax       = 0x00000100,
  NasmSyntax        = 0x00000200,
  ATSyntax          = 0x00000400,

  /* === mask = 0xff0000 */
  PrefixedNumeral   = 0x00010000,
  SuffixedNumeral   = 0x00000000,

  /* === mask = 0xff000000 */
  ShowSegmentRegs   = 0x01000000
}

// -----------------------------------------------------------------------------
